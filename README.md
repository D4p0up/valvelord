# Valvelord

Tube overdrive pedal based on Matsumin Valvecaster with optimisations from various prototype trials.

- PCB design on Kicad
- Pedal Mockup on Freecad